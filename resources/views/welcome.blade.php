<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<script src="{{ URL::asset('js/addentry.js') }}"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Skill test</div>
				          <div class="tab-pane active" id="1">
            <form method="post" class="tab-pane-form"  >
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
              <h4>Form</h4>
              <div class="form-group">
          Product name:  <input type="text" name="Pname" id="Pname"><br>
		  Quantity:	<input type="number" name="PQuantity" id="PQuantity"><br>
		  Price:	<input type="number" name="Price" id="Price"><br>
         <button type="submit" id="AddEntry" class="btn btn-success">Add Entry</button>
          </form>
          </div>
            </div>
        </div>
		<div id="display">
		<table>
		<tr>
		<th>Product name</th><th>Quantity in stock</th><th>Price per item</th><th>Datetime submitted</th><th>Total value number</th>
		</tr>
		</table>
		</div>
		</div>
    </body>
</html>
